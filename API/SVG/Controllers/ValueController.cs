﻿using Microsoft.AspNetCore.Mvc;
using SVG.Data;
using SVG.Entity;

namespace SVG.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ValueController : ControllerBase
    {
        [HttpGet]
        public ActionResult<Dimention> Get()
        {      
            return SvgData.GetData();
        }

        [HttpPost]
        public ActionResult<Dimention> Update(Dimention data)
        {
            return SvgData.SetData(data);
        }
    }
}
