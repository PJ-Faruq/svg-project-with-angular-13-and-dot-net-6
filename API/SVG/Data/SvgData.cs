﻿using Microsoft.AspNetCore.Mvc;
using SVG.Entity;
using System.Text.Json;

namespace SVG.Data
{
    public class SvgData
    {
        public static Dimention GetData()
        {

            var jsonData = File.ReadAllText("Data/data.json");
            var data = JsonSerializer.Deserialize<Dimention>(jsonData);

            return data;
        }

        public static Dimention SetData(Dimention data)
        {
            
            string output = JsonSerializer.Serialize(data);
            File.WriteAllText("Data/data.json", output);
            return data;
        }
    }
}
