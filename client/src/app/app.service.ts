import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IData } from './model/data';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  BASE_URL = 'https://localhost:7268/api/';
  constructor(private httpClient: HttpClient) {}

  getValue() {
    return this.httpClient.get<IData>(this.BASE_URL + 'Value');
  }

  setValue(model:any) {
    return this.httpClient.post<IData>(this.BASE_URL + "Value", model);
  }
}
