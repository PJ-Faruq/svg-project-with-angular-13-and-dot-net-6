import { Component, HostListener, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { IData } from './model/data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'svg-project';
  height = 200;
  width = 400;
  prevX = 0;
  prevY = 0;
  isMouseDown = false;
  perimeter = 0;

  constructor(private appService:AppService) {
    
    //this.calculatePerimeter();
  }

  ngOnInit(): void {
    this.appService.getValue().subscribe(
      response => {
        this.height = response.height;
        this.width = response.width;
        this.calculatePerimeter();
        
      }
    )
    
  }

  onMouseDown(event: any) {
    this.prevY = event.clientY;
    this.prevX = event.clientX;
    this.isMouseDown = true;
  }

  onMouseUp(event: any) {
    this.isMouseDown = false;

    this.appService.setValue({ "height": this.height, "width": this.width }).subscribe(
      response => {
        
      }
    )
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    if (!this.isMouseDown) {
      return;
    }

    this.resizHeight(event.clientY - this.prevY);
    this.prevY = event.clientY;

    this.resizWidth(event.clientX - this.prevX);
    this.prevX = event.clientX;
  }

  resizHeight(offsetY: number) {
    
    this.height += offsetY;

    // if (this.height < 200) {
    //   this.height = 200;
    // }
    this.calculatePerimeter();
  }

  resizWidth(offsetX: number) {
    
    this.width += offsetX;
    this.calculatePerimeter();
  }

  calculatePerimeter() {
    this.perimeter = 2 * (this.height + this.width);
  }
}
