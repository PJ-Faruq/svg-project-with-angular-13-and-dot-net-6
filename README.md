# SVG Project with Angular 13 and .NET 6

SVG Resizable Rectangular Project with Angular 13 and .NET 6

## Getting started

Step-1: Clone this project into your machine/PC

Step-2: Open the "API" project using Visual Studio and run it. you will get an API URL.

Step-3: Now Open the "client" (Angular) project using Visual Studio Code. run the command "npm-install".

Step-4: Go to the "app.service.ts" file.
        Modify the following line.Just Change the port number with your api port.
        "BASE_URL = 'https://localhost:7268/api/';"

Step-5: Now run the client project

Result: You can now see the result. the initial value comes from the api. You can resize the rectangular
        by dragging it. Perimeter is shown below the rectangular. After resizing the dimention value save in the backend. if you reload the page you can see the exact size as last time you modified. you can see the value is continuous changing in the json file.
        the json file location is "API/SVG/Data/data.json".
